package pl.training.What_is_new_in_Java_8.ad2_Stream_API_and_Collectors;

import java.util.Arrays;
import java.util.List;

/**
 * Created by x on 2017-09-25.
 */
public class FlatMapExercise {

    public static void main(String[] args) {

        List<Integer> lista1 = Arrays.asList(1,2,3,4,5,6,7);
        List<Integer> lista2 = Arrays.asList(2,4,6);
        List<Integer> lista3 = Arrays.asList(3,5,7);

        List<List<Integer>> listList = Arrays.asList(lista1, lista2, lista3);
        // lista z listami, jako parametry prekazujemy listy lista1, lista2, lista3

        System.out.println(listList);

        listList.stream()
                .map(m->m.size())
                .forEach(System.out::println); // wielkość wsadzonych list
    }
}
