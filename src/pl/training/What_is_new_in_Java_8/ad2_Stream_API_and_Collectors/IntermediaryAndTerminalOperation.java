package pl.training.What_is_new_in_Java_8.ad2_Stream_API_and_Collectors;

import com.sun.org.apache.xpath.internal.SourceTree;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Created by x on 2017-09-21.
 */
public class IntermediaryAndTerminalOperation {
    public static void main(String[] args) {

        // różnica pomiędzy operacjami pośrednimi(Intermadiary) a finalnymi ( final)
        // tylko operacje finalne mogą wywowałać przetwarzanie danych na streamie!

        Stream<String> strim = Stream.of("one", "two", "three", "four", "five");
        Stream<String> strim2 = Stream.of("one", "two", "three", "four", "five");

        Predicate<String> p1 = Predicate.isEqual("two");
        Predicate<String> p2 = Predicate.isEqual("three");


        ArrayList<String> lista = new ArrayList<>();


        strim
                .peek(System.out::println)
                .filter(p1.or(p2))
                .peek(lista::add); // zwraca stream, a stream nic nie przechowuje
//         .forEach(lista::add);   //- jeżeli będzie to to wyrzucu pełby wynik.

        System.out.println("Done!");
        System.out.println("wielkość listy: " + lista.size());

//zakładamy że wcześniej mam klasę Person z osobami i ich wiekiem List<Person> persons = ... ;
//
//        Optional<Integer> minAge = persons.stream()
//                .map(m->m.getAge()) // Stream <Integer>
//                .filter(f->f >20) // Stream <Integer>
//                .min(Comparator.naturalOrder()); // terminal operation, kończy operacje
//
//        person.stream()
//                .map.(p -> p.getLastName())
//                .allMatch(lenght<20); // terminal operation, kończy operacje



//
    }
}
