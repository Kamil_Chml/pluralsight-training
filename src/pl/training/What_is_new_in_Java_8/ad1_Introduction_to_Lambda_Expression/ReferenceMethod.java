package pl.training.What_is_new_in_Java_8.ad1_Introduction_to_Lambda_Expression;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

/**
 * Created by x on 2017-09-13.
 */
public class ReferenceMethod {
    public static void main(String[] args) {

        List< String> strings =  Arrays.asList("one", "two", "three", "four", "five");

        List<String> result = new ArrayList<>();

        Consumer<String> c1 = System.out::println; // przekazujemy jako parametr do forEach

        Consumer<String> c2 = result::add;

        strings.forEach(c1.andThen(c2)); /// drukuje zawartość strings

        System.out.println("Wielkosc listy result : " + result.size()); // sprawdzamy czy prawidłowo dodało zawartość strings do result

    }
}
