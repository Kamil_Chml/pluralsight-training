package pl.training.What_is_new_in_Java_8.ad1_Introduction_to_Lambda_Expression;

import java.util.*;

/**
 * Created by x on 2017-09-11.
 */
public class RunnableLambda {

    public static void main(String[] args) throws InterruptedException {

//        Runnable runnable = new Runnable() {
//            @Override
//            public void run() {
//
//                for (int i = 0; i < 3; i++) {
//
//                    System.out.println("Hello world from thread [" + Thread.currentThread().getName() + "]");
//
//                }
//
//            }
//        };


        Runnable runnableLambda = () -> {

            for (int i = 0; i < 3; i++) {

                System.out.println("Hello world from thread [" + Thread.currentThread().getName() + "]");

            }

        };




        Thread t = new Thread(runnableLambda);

        t.start(); // rozpoczyna wątek
        t.join(); // czeka na koniec egzekujcji teog wątku



//        Comparator<String> comp = new Comparator<String>() {
//            @Override
//            public int compare(String o1, String o2) {
//                return Integer.compare(o1.length(), o2.length());
//            }
//        };

        Comparator<String> compLambda = (String s1, String s2) -> Integer.compare(s1.length(), s2.length());

        List<String> list = Arrays.asList("***", "**","****", "*");
        Collections.sort(list,compLambda);

        for (String s : list){
            System.out.println(s);
        }
    }




}
